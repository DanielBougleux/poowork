import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

public abstract class Asteroid {
    //Vida do asteroide
    private int life;

    //Posição em cada frame do asteroide
    private Vector2f pos;

    //Velocidade do asteroide
    private Vector2f speed;

    //Diametro do asteroide
    private int d;

    //Imagem que representará o asteroide na tela
    private Image img;

    //Angulo de movimento do asteroide
    private double angle;

    public Asteroid(Vector2f v) {
        pos = v;
    }

    protected void naturalMove(int t) {
        //Atualiza a posição caso atravesse a janela
        pos = Colision.crossWindow(pos, d);

        //Cria velocidade real escalonando com a deltatime
        Vector2f realSpeed = speed.copy();
        pos.add(realSpeed.scale(t/1000.f));
    }

    //Método para quando atinge o asteroide
    public void gotHit(int damage) {
        life-=damage;
    }

    //Método para checar se o asteroide está vivo
    public boolean isAlive() {
        if(life > 0) return true;
        else return false;
    }

    //Gera um vetor velocidade baseado num escalar e angulo
    protected Vector2f genSpeed(double angle, int scale) {
        //para controlar o angulo de cada asteroide e rapidez
        Vector2f v = new Vector2f((float)Math.sin(angle)*scale,
                (float)Math.cos(angle)*(-scale));
        return v;
    }

    /***
     *   GETTERS E SETTERS
     *   GETTERS E SETTERS
     *   GETTERS E SETTERS
     *                     ***/

    protected void setLife(int l) {
        life = l;
    }

    public int getLife() {
        return life;
    }

    public Vector2f getCenter() {
        return new Vector2f(pos.x + getRadius(), pos.y + getRadius());
    }

    public Vector2f getPos() {
        return pos;
    }

    public Vector2f getSpeed() {
        return speed;
    }

    protected void setSpeed(Vector2f v) { speed = v; }

    protected void setD(int diameter) {
        d = diameter;
    }

    public int getD() {
        return d;
    }

    public int getRadius() {
        return d/2;
    }

    public Image getImg() {
        return img;
    }

    protected void setImg(String src) {
        try{
            this.img = new Image(src);
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    public float getX() {
        return pos.x;
    }

    public float getY() {
        return pos.y;
    }

    public double getAngle() { return angle; }

    protected void setAngle(double angle) {
        this.angle = angle;
    }

}
