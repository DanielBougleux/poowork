import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import java.util.ArrayList;
import java.util.Iterator;

public class Gun {
    //Lista de balas
    private ArrayList<Bullet> bullets;

    //Contador do deltatime para controlar taxa de tiro
    private int delta;

    //Taxa de tiro
    private static int FIRE_RATE;

    //Nave que atira
    private Ship ship;

    public Gun (Ship ship) {
        //Inicia lista de balas
        bullets = new ArrayList<>();
        delta = 0;
        this.ship = ship;
        FIRE_RATE = ship.getFireRate();
    }

    //Chama o método de renderização para todas as balas
    public void renderBullets(GameContainer gc, Graphics g) throws SlickException {
        for(Bullet b : bullets) b.render(gc, g);
    }

    //Método de atualização das balas
    public void updateBullets(int t) {
        //Itera pela lista de balas com remoção
        for(Iterator<Bullet> bllt = bullets.iterator(); bllt.hasNext(); /**/) {

            //Recebe a bala da atual iteração
            Bullet b = bllt.next();

            //Atualiza a bala
            b.update(t);
            if(!b.isActiv()) bllt.remove();
        }
    }

    //Método que controla gatilho da arma
    public void gunFire(GameContainer gc, int t) {
        //Soma o deltatime
        delta += t;

        //Se o jogador apertar espaço, e não tiver ultrapassado taxa de tiros
        if(delta > FIRE_RATE && gc.getInput().isKeyDown(Input.KEY_SPACE)) {

            //Cria nova bala para a lista bullets
            bullets.add(new Bullet(new Vector2f(ship.shotSpawnX(), ship.shotSpawnY()),
                    new Vector2f(ship.shotSpeedX(), ship.shotSpeedY()) )
            );

            //Reseta o contador de deltatime
            delta = 0;
        }
    }

    public ArrayList<Bullet> getBullets() {
        return bullets;
    }
}