import java.io.FileNotFoundException;
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

import java.io.RandomAccessFile;    
import java.io.IOException;


public class Recordes extends BasicGameState{
    private int[] scores;
    private RandomAccessFile arq;
    private long inicio_arq;
    private float x, y;
    private Image background;
    private int ID;
    private AngelCodeFont fonte; 
    private Color cor; 
    public Recordes(int state){
        ID = state;
    }
    
    public void init(GameContainer gc, StateBasedGame estado) throws SlickException{
        x = (gc.getWidth()-100)/2;
        y = 40;
        cor = new Color(47, 198, 69);
        scores = new int[10];
        background = new Image("Images/Planos de Fundo/PlanoRecorde.png");
        le_recordes();
        fonte = new AngelCodeFont("Arquivos/fonte.fnt", "Images/fonte_0.png");
    }
    
    public void render(GameContainer gc, StateBasedGame estado, Graphics g) throws SlickException{     
        background.draw((gc.getWidth()-background.getWidth())/2, (gc.getHeight()-background.getHeight())/2);
        fonte.drawString(30, y, "Recordes:", cor);
        
        for(int i = 0; i < 10; i++){
            fonte.drawString(x, y+(i+1)*42, i+1 +". " + scores[i], cor);
        }
        fonte.drawString(100, gc.getHeight()-60, "Pressione ESC para voltar ao menu principal", cor);
    }
    
    
    
    public void update(GameContainer gc, StateBasedGame estado, int delta) throws SlickException{
        Input input = gc.getInput();
        if(input.isKeyDown(Input.KEY_ESCAPE)){
            estado.enterState(0);
        }
    }
    
    public int getID(){
        return ID;
    }
    
    public void le_recordes(){
        try {
            arq = new RandomAccessFile("Arquivos/recordes.txt", "r");
            for(int i = 0; i < 10; i++){
                scores[i] = arq.readInt();
            }
            arq.close();
        }
        catch (FileNotFoundException ex) {
            System.out.print(ex);
        }
        catch (IOException ex) {
            System.out.print(ex);
        }
        catch (NumberFormatException ex){
            System.out.println(ex);
        }
    }
    
    public boolean eh_recorde(int pontuacao){
        this.le_recordes();
        
        if(pontuacao > scores[9]){
            return true;
        }
        else{
            return false;
        }
    }
    
    public void inseri_recorde(int pontuacao){
        this.le_recordes();
        for(int i = 0; i < 10; i++){
            if (pontuacao == scores [i]){
                return;
            }
            
            if(pontuacao > scores[i]){
                int temp;
                for(int j = i; j < 10; j++){
                    temp = scores[j];
                    scores[j] = pontuacao;
                    pontuacao = temp;
                }
                break;
            }
        }
        
        //Atualizando o arquivo de recordes
        this.escreve_recordes();
    }
    
    public void escreve_recordes(){
        try {
            arq = new RandomAccessFile("Arquivos/recordes.txt", "rwd");
            for(int i = 0; i < 10; i++){
                arq.writeInt(scores[i]);
            }
            arq.close();
        }
        catch (FileNotFoundException ex) {
            System.out.print(ex);
        }
        catch (IOException ex) {
            System.out.print(ex);
        }
    }
}

