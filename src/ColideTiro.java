import org.newdawn.slick.*;

public class ColideTiro implements Runnable {

    AsteroidController asteroidController;
    Gun gun;

    public ColideTiro(AsteroidController asteroidController, Gun gun){
        this.asteroidController = asteroidController;
        this.gun = gun;
    }

    public void run(){
        asteroidController.smashAsteroids(gun.getBullets());
    }

}
