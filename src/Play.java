import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.*;
import sun.awt.windows.ThemeReader;

import java.util.ArrayList;

public class Play extends BasicGameState {

    int ID;

    //Nave do jogador
    private Ship Ship;

    //Controlador de tiros
    private Gun gun;

    //Altura e largura da janela
    private int wHeight = 600, wWidth = 800;

    //Lista de imagem de vidas para mostrar na tela
    private ArrayList<Image> lives;

    //Controlador de asteroides
    private AsteroidController asteroidController;

    //Inicia o jogo no nível 1
    private int nivel;

    private int nave;

    private Image background;

    private Image endgame;

    private boolean invunerable;

    private boolean deadShip;

    private Animation navemorta;

    private boolean paused;

    private Vector2f posNaveMorta;

    private long marcaTempo;

    private long tempoAtual;

    private boolean gameOver;

    private boolean nextLvl;

    private Image voltar;

    private Image menu;


    public Play(int state, int nave){

        ID = state;
        nivel = 1;
        this.nave = nave;

    }

    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {

        //Inicializa nave
        setNave(nave);



        //Inicializa lista de vidas do jogador
        lives = new ArrayList<>();

        //Adiciona vidas na lista de vidas baseado na vida atual do jogador
        int i;
        for(i=Ship.getLife()-1; i>=0; i--)lives.add(new Image("/Images/Sprites Naves/vida.png"));

        //Inicia controlador de asteroides
        asteroidController = new AsteroidController(nivel, Ship);

        //Liga os asteroides
        asteroidController.turnOnAsteroids();

        background = new Image("Images/Planos de Fundo/Falatu.jpg");

        endgame = new Image("Images/Planos de Fundo/gameover.png");

        invunerable = true;

        paused = false;

        deadShip = false;

        gameOver = false;

        nextLvl = false;

        marcaTempo = System.currentTimeMillis();

        voltar = new Image("Images/Botões/Voltar.png");

        menu = new Image("Images/Botões/Menu.png");

    }

    public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException{

        background.draw((gc.getWidth()-background.getWidth())/2,
                (gc.getHeight()-background.getHeight())/2);

        //Desenha a nave
        if(!deadShip){
            Ship.getImg().draw(Ship.getX(), Ship.getY());
        }

        //Espaço entre os corações "lives"
        int esp=8;
        //Desenha os corações com espaçamento
        for( Image l : lives) { l.draw(esp+1,30); esp+=20; }

        //Renderiza todas as balas
        gun.renderBullets(gc, g);

        //Renderiza cada um dos asteroides
        float astx, asty;
        for(Asteroid a : asteroidController.getAsteroides()) {
            astx = a.getX();
            asty = a.getY();
            a.getImg().draw(astx, asty);
        }

        if(deadShip){
            navemorta.draw(posNaveMorta.x,posNaveMorta.y);
        }

        if(gameOver){
            endgame.draw((gc.getWidth()-endgame.getWidth())/2,
                    (gc.getHeight()-endgame.getHeight())/2);
        }

        if(nextLvl){
            g.drawString("Nível:" + nivel, gc.getWidth()/2,gc.getHeight()/2);
        }

        //Desenha marcador de pontuação na tela
        g.drawString("SCORE: " + asteroidController.getScore(), 9, 55);

        if(paused){
            menu.draw((gc.getWidth()-menu.getWidth())/2,(gc.getHeight()-menu.getHeight()-voltar.getHeight())/2);
            voltar.draw((gc.getWidth()-menu.getWidth())/2,((gc.getHeight()-menu.getHeight()-voltar.getHeight())/2)
                    +menu.getHeight());
        }

    }

    public void update(GameContainer gc, StateBasedGame sbg, int delta)throws SlickException{

        Input input = gc.getInput();

        if(input.isKeyDown(Input.KEY_ESCAPE)){
            paused = true;
        }

        if(paused){
            if(input.isKeyDown(Input.KEY_M)){
                sbg.enterState(0);
            }else if(input.isKeyDown(Input.KEY_V)){
                paused = false;
            }
        }

        if(asteroidController.isOver()){
            nextLvl(gc,sbg);
        }

        if(nextLvl){
            tempoAtual = System.currentTimeMillis();
            if(tempoAtual - marcaTempo >=3000){
                nextLvl = false;
            }
        }

        if(!nextLvl){
            if(Ship.getLife() <= 0 && !gameOver){
                gameOver = true;
                Recordes tempor = (Recordes) sbg.getState(2);
                if(tempor.eh_recorde(asteroidController.getScore())){
                    tempor.le_recordes();
                    tempor.inseri_recorde(asteroidController.getScore());
                    tempor.escreve_recordes();
                }
                marcaTempo = System.currentTimeMillis();
            }

            if(gameOver){
                tempoAtual = System.currentTimeMillis();
                if(tempoAtual - marcaTempo >= 5000){
                    sbg.enterState(0);
                }
            }

            if(!gameOver){

                if(deadShip){
                    int j;
                    for(j =0; j<5;j++){
                        navemorta.getImage(j).rotate(delta/4);
                    }
                    tempoAtual = System.currentTimeMillis();
                    if(tempoAtual - marcaTempo >= 2500){
                        deadShip = false;
                        invunerable = true;
                        Ship.resetPos(gc);
                        Ship.resetSpeed();
                        marcaTempo = System.currentTimeMillis();
                    }
                }

                if(invunerable){
                    tempoAtual = System.currentTimeMillis();
                    if(tempoAtual-marcaTempo >= 3000){
                        invunerable = false;
                    }
                }

                if (!deadShip && !paused){

                    //Atualiza posição dos asteroides
                    asteroidController.moveAsteroids(delta);

                    //Controla tiro da nave
                    gun.gunFire(gc, delta);

                    //Controla movimento da nave
                    Ship.Joystick(gc, delta);

                    //Atualiza balas
                    gun.updateBullets(delta);

                    Thread t1;
                    Thread t2;

                    //Testa colisão da nave com os asteroides
                    if(!invunerable){
                        t1 = new Thread(new ColideNave(asteroidController, Ship, lives, this));
                        t1.start();
                        try {
                            t1.join();
                        }catch (InterruptedException i){
                            i.printStackTrace();
                        }
                    }

                    //Testa destruição dos asteroides
                    t2 = new Thread(new ColideTiro(asteroidController,gun));
                    t2.start();
                    try {
                        t2.join();
                    }catch (InterruptedException i){
                        i.printStackTrace();
                    }
                    //asteroidController.smashAsteroids(gun.getBullets());

                    asteroidController.destroyAsteroid();

                }
            }
        }

    }

    public int getID(){
        return ID;
    }

    public void setNave(int nave)throws SlickException{
        this.nave = nave;
        if(nave == 0){
            Ship = new Spectroscope(wWidth, wHeight);
            String[] ship0 = {"Images/Sprites Naves/deathShips/spectroscope1.png",
                    "Images/Sprites Naves/deathShips/spectroscope2.png",
                    "Images/Sprites Naves/deathShips/spectroscope3.png",
                    "Images/Sprites Naves/deathShips/spectroscope4.png",
                    "Images/Sprites Naves/deathShips/imgfinal.png"};
            Image[] shipI = new Image[5];
            for(int i=0;i<5;i++){
                shipI[i] = new Image(ship0[i]);
            }
            navemorta = new Animation(shipI, 400, true);
        }else if(nave == 1){
            Ship = new Stinger(wWidth, wHeight);
            String[] ship0 = {"Images/Sprites Naves/deathShips/stinger1.png","Images/Sprites Naves/deathShips/stinger2.png",
                    "Images/Sprites Naves/deathShips/stinger3.png", "Images/Sprites Naves/deathShips/stinger4.png",
                    "Images/Sprites Naves/deathShips/imgfinal.png"};
            Image[] shipII = new Image[5];
            for(int i=0;i<5;i++){
                shipII[i] = new Image(ship0[i]);
            }
            navemorta = new Animation(shipII, 400, true);
        }else  if(nave == 2){
            Ship = new JUGGER(wWidth, wHeight);
            String[] ship0 = {"Images/Sprites Naves/deathShips/jugger1.png","Images/Sprites Naves/deathShips/jugger2.png",
                    "Images/Sprites Naves/deathShips/jugger3.png", "Images/Sprites Naves/deathShips/jugger4.png",
                    "Images/Sprites Naves/deathShips/imgfinal.png"};
            Image[] shipIII = new Image[5];
            for(int i=0;i<5;i++){
                shipIII[i] = new Image(ship0[i]);
            }
            navemorta = new Animation(shipIII, 400, true);
        }
        gun = new Gun(Ship);
    }

    public void morreu(){

        deadShip = true;
        posNaveMorta = new Vector2f(Ship.getX(), Ship.getY());
        navemorta.start();
        navemorta.restart();
        navemorta.stopAt(4);
        marcaTempo = System.currentTimeMillis();
    }

    private void nextLvl(GameContainer gc, StateBasedGame sbg)throws SlickException{
        nivel++;
        int oldvidas = Ship.getLife();
        this.init(gc,sbg);
        nextLvl = true;
        for(int i = 0; i<3-oldvidas;i++){
            Ship.gotHit(lives);
        }
        marcaTempo = System.currentTimeMillis();
    }

    public void reset(GameContainer gc, StateBasedGame sbg)throws SlickException{
        nivel = 0;
        this.nextLvl(gc,sbg);
    }
}