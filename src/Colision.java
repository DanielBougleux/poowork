import org.newdawn.slick.geom.Vector2f;

public class Colision {

    //Colisão entre circulos
    public static boolean circle(Vector2f centro1, Vector2f centro2,
                                 int raio1, int raio2) {
        //Calcula distancia entre os centros por pitagoras
        float d = (centro1.x-centro2.x)*(centro1.x-centro2.x)
                + (centro1.y-centro2.y)*(centro1.y-centro2.y);

        //Distancia é sempre um valor positivo
        d = Math.abs(d);

        //Calcula a hipotenusa quando não há colisão em pitagoras
        float r = (raio1+raio2)*(raio1+raio2);
        r = Math.abs(r);

        boolean collided = false;
        if(d < r) collided = true;
        return collided;
    }

    //Calcula colisão com o topo da janela
    public static boolean topWindow(Vector2f v) {
        boolean collided = false;

        //Se estiver saindo pra cima
        if(v.y < 0) collided = true;
        return collided;
    }

    //Calcula colisão com o chão da janela
    public static boolean botWindow(int height, Vector2f v, int diameter) {
        boolean collided = false;

        //Se estiver saindo pra baixo da janela
        if(v.y+diameter > height) collided = true;
        return collided;
    }

    //Calcula colisão com a esquerda da janela
    public static boolean leftWindow(Vector2f v) {
        boolean collided = false;

        //Se estiver saindo pra esquerda da janela
        if(v.x < 0) collided = true;
        return collided;
    }

    //Calcula colisão com a direita da janela
    public static boolean rightWindow(int width, Vector2f v, int diameter) {
        boolean collided = false;

        //Se estiver saindo para a direita da janela
        if(v.x+diameter > width) collided = true;
        return collided;
    }

    //Método que atualiza posição caso atravesse janela
    public static Vector2f crossWindow(Vector2f p, int d) {
        //Testa colisão com o chão, e atualiza a posição para o topo da janela
        if(Colision.botWindow(600, p, d)) p.set(p.x, 1.0f);

        //Testa colisão com o topo, e atualiza posição para o chão da janela
        else if(Colision.topWindow(p)) p.set(p.x, 600-1-d);

        //Testa colisão com a esquerda, e atualiza posição para a direita da janela
        if(Colision.leftWindow(p)) p.set(800-1-d, p.y);

        //Testa colisão com a direita, e atualiza posição para a esquerda da janela
        else if(Colision.rightWindow(800,p,d))p.set(1.0f,p.y);
        return p;
    }

    public static boolean circleTriangle(Ship ship, Asteroid a) {
        Vector2f vetices[] = ship.getVertices();

        //Vertice dentro do circulo

        float c0x = vetices[0].x - a.getCenter().x;
        float c0y = vetices[0].y - a.getCenter().y;

        float radiussqr = a.getRadius()*a.getRadius();

        float c0sqr = c0x*c0x + c0y*c0y - radiussqr;

        if(c0sqr <= 0) return true;

        float c1x = vetices[1].x - a.getCenter().x;
        float c1y = vetices[1].y - a.getCenter().y;
        float c1sqr = c1x*c1x + c1y*c1y - radiussqr;

        if(c1sqr <= 0) return true;

        float c2x = vetices[2].x - a.getCenter().x;
        float c2y = vetices[2].y - a.getCenter().y;
        float c2sqr = c2x*c2x + c2y*c2y - radiussqr;

        if(c2sqr <= 0) return true;

        //Aresta dentro do circulo

        float e0x = vetices[1].x - vetices[0].x;
        float e0y = vetices[1].y - vetices[0].y;

        float k = c0x*e0x + c0y*e0y;

        if(k > 0) {
            float len = e0x*e0x + e0y*e0y;

            if(k < len) {
                if(c0sqr *len <= k*k) return true;
            }
        }

        float e1x = vetices[2].x - vetices[1].x;
        float e1y = vetices[2].y - vetices[1].y;

        k = c1x*e1x + c1y*e1y;

        if(k > 0) {
            float len = e1x*e1x + e1y*e1y;

            if(k < len) {
                if(c1sqr * len <= k*k) return true;
            }
        }

        float e2x = vetices[0].x - vetices[2].x;
        float e2y = vetices[0].y - vetices[2].y;

        k = c2x*e2x + c2y*e2y;

        if(k > 0) {
            float len = e2x*e2x + e2y*e2y;

            if(k < len) {
                if(c2sqr * len <= k*k) return true;
            }
        }

        return false;
    }

}
