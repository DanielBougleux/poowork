import org.newdawn.slick.geom.Vector2f;

public class PeqAsteroid extends Asteroid{
    public PeqAsteroid(Vector2f v, double angle, boolean sum, int life){
        //Atribui vetor posição
        super(v);

        //Atribui vida do asteroide pequeno
        setLife(life);

        //Atribui imagem do asteroide
        setImg("Images/Sprites Asteroids/menorasteroide.png");

        //Atribui o diametro do asteroide pequeno
        setD(21);

        //Cada asteroide começa se movendo numa direção aleatória
        setSpeed(genSpeed(spawnAngle(angle, sum), 150));
    }

    private double spawnAngle(double angle, boolean sum) {
        double quarterOfPi = Math.PI / 4;

        if(sum)setAngle(angle + quarterOfPi);
        else setAngle(angle - quarterOfPi);

        return getAngle();
    }
}
