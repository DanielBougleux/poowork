import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import java.util.ArrayList;

public abstract class Ship {

    //Imagem da nave
    private Image img;

    //Coordenadas da nave
    private Vector2f pos;

    //vetor de velocidade da nave
    private Vector2f speed;

    //velocidade da nave
    private float nspeed;

    private int damage;

    private int fireRate;

    //Escalar de rapidez
    private float escalar;

    //Vidas da nave
    private int life;

    private int r;

    private Vector2f vertices[];

    private float actSpeedX;

    private float actSpeedY;

    private boolean virou;

    private boolean naoAcelera;

    public Ship(String src, int width, int height, float vel, int damage, int fireRate, int raio) {

        //Imagem da nave
        try { img = new Image(src); }
        catch(SlickException e) { e.printStackTrace(); }

        //Recebe largura e altura da janela
        float w = (float)width, h = (float)height;

        //Atribui posição para metade da tela
        pos = new Vector2f(w/2, h/2);

        nspeed = vel;

        this.damage = damage;

        this.fireRate = fireRate;

        //Inicializa variaveis padrões
        speed = new Vector2f(0.0f, 0.0f);
        escalar = 0.1f;

        r = raio;

        life = 3;
        vertices = new Vector2f[3];

        virou = false;
    }

    //Método para controlar movimento com input do usuário
    public void Joystick(GameContainer gc, int t) {
        Input input = gc.getInput();

        if(!virou){
            actSpeedX = getSpeedX(escalar);
            actSpeedY = getSpeedY(escalar);
        }

        naoAcelera = true;

        //Se apertar seta para cima
        if (input.isKeyDown(input.KEY_UP) || input.isKeyDown(Input.KEY_DOWN)) {

            //Atribui velocidade com escalar
            speed.set(actSpeedX * t, actSpeedY * t);

            //Aumenta escalar de rapidez
            escalar += t/nspeed;

            //Adiciona a velocidade à posição
            pos.add(speed);

            virou = false;
            naoAcelera = false;
        }

        //Quando o botão é solto, desacelera
        else {

            //Reduz o escalar de rapidez
            if(escalar > 0){
                escalar -= t/(nspeed+3000);
            }
            if(escalar < 0) escalar = 0;

            //Atribui velocidade com escalar
            speed.set(actSpeedX * t, actSpeedY * t);

            speed.scale(escalar);

            //Adiciona a velocidade à posição
            pos.add(speed);

        }

        //Atualiza posição da nave caso atravesse janela
        pos = Colision.crossWindow(pos, getD());

        //Roda sentido horário, baseado no deltatime
        if(input.isKeyDown(input.KEY_RIGHT) && naoAcelera) {
            img.rotate(t/3);
            virou = true;

        }//Roda sentido anti-horário, baseado no deltatime
        else if(input.isKeyDown(input.KEY_LEFT) && naoAcelera){
            img.rotate(-t/3);
            virou = true;
        }
    }


    //Retorna velocidade da nave baseado no angulo de rotação e escalar
    private float getSpeedX(float escalar) {
        return (float)Math.sin(Math.toRadians(img.getRotation()))*(escalar);
    }
    private float getSpeedY(float escalar) {
        return (float)Math.cos(Math.toRadians(img.getRotation()))*(-escalar);
    }

    //Retorna velocidade do tiro baseado no angulo de rotação da nave e escalar
    public float shotSpeedX() {
        return (float)Math.sin(Math.toRadians(img.getRotation()))*360;
    }
    public float shotSpeedY() {
        return (float)Math.cos(Math.toRadians(img.getRotation()))*(-360);
    }


    //Retorna onde a bala deve surgir
    public float shotSpawnX() {
        float sen = (float)Math.sin(Math.toRadians(img.getRotation()));
        //(Posição no circulo) + centro X do eixo da img + posX da img na janela
        return (sen*r) + img.getCenterOfRotationX() + pos.x;
    }

    public float shotSpawnY() {
        float cos = (float)Math.cos(Math.toRadians(img.getRotation()))*(-1);
        //(Posição no circulo) + centro Y do eixo da img + posY da img na janela
        return (cos*r) + img.getCenterOfRotationY() + pos.y;
    }

    private float leftBottomX() {
        float sen = (float)Math.sin(Math.toRadians(img.getRotation()+245));
        //(Posição no circulo) + posX da img na janela
        return (sen*r) + img.getCenterOfRotationX() + pos.x;
    }

    private float bottomY() {
        float cos = (float)Math.cos(Math.toRadians(img.getRotation()));
        //(Posição no circulo) + diametro da imagem + posY da img na janela
        return (cos*r) + getR() + pos.y;
    }

    private float rightBottomX() {
        float sen = (float)Math.sin(Math.toRadians(img.getRotation()+115));
        //(Posição no circulo) + diametro da imagem + posX da img na janela
        return (sen*r) + img.getCenterOfRotationX() + pos.x;
    }

    public void gotHit(ArrayList<Image> lives) {
        if(!lives.isEmpty()) {
            lives.remove(life-1);
            life--;
        }
    }

    /***
    * GETTERS E SETTERS
    * GETTERS E SETTERS
    * GETTERS E SETTERS ***/

    public int getD() { return r*2; }
    public int getR() { return r; }
    public Vector2f getPos() { return pos; }
    public int getLife() { return life; }
    public Image getImg() { return img; }
    public float getX() { return pos.x; }
    public float getY() { return pos.y; }
    public int getFireRate(){return fireRate;}
    public int getDamage(){return damage;}
    public void resetPos(GameContainer gc){
        pos = new Vector2f(gc.getWidth()/2, gc.getHeight()/2);
    }
    public void resetSpeed(){
        speed = new Vector2f(0.0f, 0.0f);
        escalar = 0.1f;
    }
    public void setLife(int life){
        this.life = life;
    }

    public Vector2f[] getVertices() {
        //Coordenadas do vértice  do topo da nave
        vertices[0] = new Vector2f(shotSpawnX(), shotSpawnY());

        //Coordenadas do vértice de baixo à esquerda
        vertices[1] = new Vector2f(leftBottomX(), bottomY());

        //Coordenadas do vértice de baixo à direita
        vertices[2] = new Vector2f(rightBottomX(), bottomY());

        return vertices;
    }

}
