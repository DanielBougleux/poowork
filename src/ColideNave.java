import org.newdawn.slick.Image;

import java.util.ArrayList;

public class ColideNave implements Runnable {

    AsteroidController asteroidController;
    Ship ship;
    ArrayList<Image> lista;
    Play jogo;

    public ColideNave(AsteroidController asteroidController, Ship ship, ArrayList<Image> lista, Play jogo){
        this.asteroidController = asteroidController;
        this.ship = ship;
        this.lista = lista;
        this.jogo = jogo;
    }

    public void run(){
        asteroidController.crashShip(ship,lista,jogo);
    }

}
