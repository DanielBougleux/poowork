import org.newdawn.slick.geom.Vector2f;

public class MedAsteroid extends Asteroid {

    public MedAsteroid(Vector2f v, double angle, boolean sum, int life){
        //Atribui vetor posição
        super(v);

        //Atribui vida do asteroide médio
        setLife(life);

        //Atribui imagem do asteroide
        setImg("Images/Sprites Asteroids/medioAsteroide.png");

        //Atribui o diametro do asteroide medio
        setD(38);

        //Cada asteroide começa se movendo numa direção aleatória
        setSpeed(genSpeed(spawnAngle(angle, sum), 120));
    }

    private double spawnAngle(double angle, boolean sum) {
        double quarterOfPi = Math.PI / 4;

        if(sum)setAngle(angle + quarterOfPi);
        else setAngle(angle - quarterOfPi);

        return getAngle();
    }

}
