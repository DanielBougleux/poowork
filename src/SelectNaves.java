import org.lwjgl.input.Mouse;
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

public class SelectNaves extends BasicGameState{

    private int ID;
    private Button[] bSetas;
    private Image[] naves;
    private int nNaves;
    private int[] booleanNaves;
    private float navesWidth;
    private float navesHeight;
    private float setasWidth;
    private float setasHeight;
    private float navesx;
    private float navesy;
    private Image background;
    private int imgAtual;
    private boolean isClicked0;
    private boolean isClicked1;


    public SelectNaves(int state, int nNaves, float navesWidth, float navesHeight
            , float setasWidth, float setasHeight){
        ID = state;
        this.navesWidth = navesWidth;
        this.navesHeight = navesHeight;
        this.setasWidth = setasWidth;
        this.setasHeight = setasHeight;
        this.nNaves = nNaves;
        naves = new Image[nNaves];
        bSetas = new Button[2];
        imgAtual = 0;
        isClicked0 = false;
        isClicked1 = false;
    }

    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {

        Naves temp = (Naves)sbg.getState(3);
        booleanNaves = temp.getBooleanNaves();

        float espacamentox = (gc.getWidth()-navesWidth-setasWidth*2)/4;
        float espacamentoysetas = (gc.getHeight()-setasHeight)/2;

        navesy = (gc.getHeight()-navesHeight)/2;
        navesx = espacamentox*2+setasWidth;

        String[] seta1 = {"Images/TelaDeNaves/BotãoSetaDNC.png", "Images/TelaDeNaves/BotãoSetaDC.png"};
        bSetas[0] = new Button(seta1, espacamentox*3+setasWidth+navesWidth, espacamentoysetas);
        String[] seta2 = {"Images/TelaDeNaves/BotãoSetaENC.png", "Images/TelaDeNaves/BotãoSetaEC.png"};
        bSetas[1] = new Button(seta2, espacamentox, espacamentoysetas);

        naves[0] = new Image("Images/TelaDeNaves/TelaNave1CC.png");
        naves[1] = new Image("Images/TelaDeNaves/TelaNave2CC.png");
        naves[2] = new Image("Images/TelaDeNaves/TelaNave3CC.png");

        background = new Image("Images/Planos de Fundo/Falatu.jpg");

    }

    public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException{
        background.draw((gc.getWidth()-background.getWidth())/2,
                (gc.getHeight()-background.getHeight())/2);
        g.drawString("Pressione ESC Para retornar ao menu\nPressione ENTER para selecionar uma nave",
                20, 20);
        g.drawImage(naves[imgAtual], navesx, navesy);
        for(Clicavel x : bSetas){
            x.draw(g);
        }
    }

    public void update(GameContainer gc, StateBasedGame sbg, int delta)throws SlickException{
        Input input = gc.getInput();
        if(input.isKeyDown(Input.KEY_ESCAPE)){
            sbg.enterState(0);
        }
        if(input.isKeyDown(Input.KEY_ENTER) || input.isKeyDown(Input.KEY_NUMPADENTER)){
            Play temp = (Play)sbg.getState(1);
            temp.setNave(imgAtual);
            temp.reset(gc,sbg);
            sbg.enterState(1);
        }

        int mousex = Mouse.getX();
        int mousey = Math.abs(gc.getHeight()-Mouse.getY());

        if(bSetas[0].clicou(mousex, mousey)){
            bSetas[0].setimgatual(1);
            isClicked0 = true;
        }else{
            bSetas[0].setimgatual(0);
            if(isClicked0){

                do{
                    if(imgAtual == nNaves-1){
                        imgAtual = 0;
                    }else {
                        imgAtual++;
                    }
                }while (booleanNaves[imgAtual] == 0);
            }
            isClicked0 = false;
        }

        if(bSetas[1].clicou(mousex, mousey)){
            bSetas[1].setimgatual(1);
            isClicked1 = true;
        }else {
            bSetas[1].setimgatual(0);
            if(isClicked1){
                do{
                    if(imgAtual == 0){
                        imgAtual = nNaves-1;
                    }else {
                        imgAtual--;
                    }
                }while (booleanNaves[imgAtual] == 0);
            }
            isClicked1 = false;
        }

    }

    public int getID(){
        return ID;
    }

    public void updateBoolean(StateBasedGame sbg){
        Naves temp = (Naves)sbg.getState(3);
        booleanNaves = temp.getBooleanNaves();
    }

}
